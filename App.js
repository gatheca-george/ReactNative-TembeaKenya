import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Button, ImageBackground } from 'react-native';
import { ToastAndroid, Platform, AlertIOS} from 'react-native';

export default function App() {
  return (
    <ImageBackground style={styles.container} source={require("./assets/splash.jpg")}>
      <Button title='Hello From Gakoji Enterprises' onPress={() => notifyMessage('clicked')}/>
      <StatusBar style="auto" />
    </ImageBackground>
  );
}

function notifyMessage(msg) {
  if (Platform.OS === 'android') {
    ToastAndroid.show(msg, ToastAndroid.SHORT)
  } else {
    AlertIOS.alert(msg);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
